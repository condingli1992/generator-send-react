declare module '*.json';

declare module '*.png';
declare module '*.svg';

declare module '*.sass' {
  const content: { [className: string]: string };
  export default content;
}

declare module '*.scss' {
  const content: { [className: string]: string };
  export default content;
}
