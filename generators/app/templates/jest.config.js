module.exports = {
  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'node',
  setupFiles: ['./__test__/Setup.js', './__mocks__/browserMock.js'],
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  testPathIgnorePatterns: ['/node_modules/'],
  testRegex: '.*\\.test\\.js$',
  testURL: 'http://localhost/',
  verbose: true,
  collectCoverage: true,
  collectCoverageFrom: [
    'src/Stores/**/*.{ts,tsx}',
    'src/Modules/**/*.{ts,tsx}'
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less|scss)$': 'identity-obj-proxy'
  }
};
