/* tslint:disable */

/**
 * webpack.config.dll
 * 提取公共 dll bundle
 */

import * as webpack from 'webpack';
import * as path from 'path';

const dllConfig = {
  context: path.resolve(__dirname, '../'),

  mode: 'production',

  name: 'vendor',

  entry: {
    core: ['react', 'react-dom', 'react-router-dom'],
    assets: ['axios', 'nprogress', 'react-loadable', 'react-motion']
  },

  output: {
    path: path.resolve(__dirname, '../build/dll'),
    filename: '[name]_[hash:7].dll.js',
    library: '[name]_[hash]'
  },

  plugins: [
    new webpack.DllPlugin({
      name: '[name]_[hash]',
      path: path.resolve(__dirname, '../build/dll/[name].manifest.json')
    }),

    // 稳定hash值
    new webpack.HashedModuleIdsPlugin()
  ]
};

export default dllConfig;
