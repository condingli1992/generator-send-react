/* tslint:disable */

/**
 * webpack.config.base
 * webpack公共配置
 */

import * as webpack from 'webpack';
import * as path from 'path';

export const PATHS = {
  src: path.join(__dirname, '../src'),
  assets: path.join(__dirname, '../src/Assets'),
  build: path.join(__dirname, '../build')
};

// 组件私有样式配置
export const cssLoaderOptions = {
  loader: 'css-loader',
  options: {
    modules: true,
    sourceMap: true,
    importLoaders: 2,
    localIdentName: '[path][name]__[local]__[hash:base64:5]'
  }
};

export const fileLoaderOptions = {
  limit: 8192,
  mimetype: 'extname',
  name: 'files/[name].[ext]'
};

const baseConfig = {
  mode: 'production',

  context: path.resolve(__dirname, '../'),

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.ts', '.scss', '.css']
  },

  entry: {
    app: path.resolve(PATHS.src, './App.tsx')
  },

  output: {
    path: PATHS.build,
    publicPath: './',
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].bundle.map',
    chunkFilename: '[name].chunk.bundle.js'
  },

  plugins: [
    // 引用预编译的dll bundle
    ...getDllManifest(['core', 'assets'])
  ],

  externals: {
    window: 'window',
    jquery: '$'
  },

  performance: {
    hints: false
  },

  module: {
    rules: [
      {
        test: /\.(tsx|ts)$/,
        use: [
          {
            loader: 'ui-component-loader',
            options: {
              lib: 'antd',
              camel2: '-',
              style: 'index.css'
            }
          },
          'ts-loader'
        ],
        include: [path.resolve(PATHS.src, './')]
      },

      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: 'images/[name].[ext]'
            }
          }
        ],
        exclude: /node_modules/
      },

      {
        test: /woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: fileLoaderOptions
          }
        ],
        exclude: /node_modules/
      },

      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: fileLoaderOptions
          }
        ],
        exclude: /node_modules/
      }
    ]
  }
};

function getDllManifest(entries: string[]) {
  return entries.map(entry => {
    return new webpack.DllReferencePlugin({
      context: path.resolve(PATHS.src, './'),
      manifest: path.resolve(PATHS.build, `./dll/${entry}.manifest.json`)
    });
  });
}

export default baseConfig;
