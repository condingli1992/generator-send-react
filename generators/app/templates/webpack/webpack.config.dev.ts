/* tslint:disable */

/**
 * webpack.config.dev
 * webpack开发配置
 */

import * as Webpack from 'webpack';
import * as path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import baseConfig, { PATHS, cssLoaderOptions } from './webpack.config.base';
const DashboardPlugin = require('webpack-dashboard/plugin');

const devConfig = {
  ...baseConfig,

  mode: 'development',

  devtool: 'eval',

  plugins: [
    ...baseConfig.plugins,

    // 启用HMR
    new Webpack.HotModuleReplacementPlugin(),
    new Webpack.NamedModulesPlugin(),
    new Webpack.NoEmitOnErrorsPlugin(),
    // 定义全局变量
    new Webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false),
      'process.env.NODE_ENV': JSON.stringify('development')
    }),

    // 格式化编译日志
    new DashboardPlugin(),

    // html模板配置
    new HtmlWebpackPlugin({
      title: 'demo',
      favicon: path.join(PATHS.assets, './icons/favicon.ico'),
      template: path.join(PATHS.src, './index.html'),
      hash: true,
      inject: 'body'
    })
  ],

  devServer: {
    publicPath: 'http://localhost:8080/',
    contentBase: PATHS.assets,
    compress: true,
    headers: {
      'X-Content-Type-Options': 'nosniff',
      'X-Frame-Options': 'DENY'
    },
    open: true,
    hot: true,
    port: 8080,
    historyApiFallback: true
  },

  module: {
    rules: [
      ...baseConfig.module.rules,

      // 组件私有样式
      {
        test: /\.(sc|sa|c)ss$/,
        use: [
          'style-loader',
          'css-modules-typescript-loader',
          cssLoaderOptions,
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ],
        include: path.resolve(PATHS.src, './'),
        exclude: [/node_modules/, path.resolve(PATHS.src, './Styles')]
      },

      // 全局样式
      {
        test: /\.(sc|sa|c)ss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ],
        include: [path.resolve(PATHS.src, './Styles')]
      }
    ]
  }
};

export default devConfig;
