/* tslint:disable */

/**
 * webpack.config.prod
 * webpack 生产配置
 */

import * as Webpack from 'webpack';
import * as path from 'path';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import baseConfig, { PATHS, cssLoaderOptions } from './webpack.config.base';

const prodConfig = {
  ...baseConfig,

  output: {
    ...baseConfig.output,
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js'
  },

  module: {
    rules: [
      ...baseConfig.module.rules,

      // 抽取私有样式文件
      {
        test: /\.(sc|sa|c)ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-modules-typescript-loader', cssLoaderOptions, 'sass-loader'],
        include: path.resolve(PATHS.src, './'),
        exclude: [/node_modules/, path.resolve(PATHS.src, './Styles')]
      },

      // 抽取全局样式
      {
        test: /\.(sc|sa|c)ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
        include: path.resolve(PATHS.src, './Styles')
      }
    ]
  },

  plugins: [
    ...baseConfig.plugins,

    new Webpack.HashedModuleIdsPlugin(),
    new CopyWebpackPlugin([{ from: PATHS.assets, to: PATHS.build }]),

    new HtmlWebpackPlugin({
      title: 'demo',
      template: path.join(PATHS.src, './index.html'),
      favicon: path.join(PATHS.assets, './icons/favicon.ico'),
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1'
        }
      ],
      inject: 'body',
      minify: {
        html5: true,
        useShortDoctype: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        preserveLineBreaks: true,
        removeComments: true,
        keepClosingSlash: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true
      },
      modile: true
    }),

    new MiniCssExtractPlugin({
      filename: '[name].[chunkhash].css'
    }),

    new Webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true),
      'process.env.NODE_ENV': JSON.stringify('production')
    }),

    new UglifyJsPlugin()
  ],

  optimization: {
    splitChunks: {
      chunks: 'initial',
      automaticNameDelimiter: '~'
    }
  }
};

export default prodConfig;
