'use strict'

const path = require('path');

// 当前目录名
const basename = path.basename(process.cwd());

/**
 * 获取当前目录名
 */
const getBasename = () => basename;

/**
 * @description 获取应用名称
 * @param  {string} appname
 */
const getAppName = (appname) => {
  if (!appname)
    appname = getBasename();
  return appname;
}

module.exports = {
  getBasename,
  getAppName
};
