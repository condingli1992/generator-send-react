/**
 * TodoList
 *
 */

import React, { FunctionComponent } from 'react';
import { observer } from 'mobx-react';
import { List } from 'antd';
import { TodoInstance } from '../../Stores/Todo';
import TodoItem from './TodoItem';
import TodoListFooter from './TodoListFooter';
import styles from './index.scss';

export interface TodoListProps {
  todo: TodoInstance;
}

const TodoList: FunctionComponent<TodoListProps> = ({ todo }) => {
  return (
    <div className={styles.todoList}>
          <List
            bordered={true}
            size="small"
            footer={
              <TodoListFooter
                len={todo.todoLength}
                filterType={todo.filterType}
                setFilterType={todo.setFilterType}
                clearCompletedItems={todo.clearCompletedItems}
              />
            }
          >
            {todo.showList.map(item => (
              <TodoItem
                key={item.id}
                todoItem={item}
                deleteTodoItem={todo.deleteTodoItem}
                updateTodoItem={todo.updateTodoItem}
                editorForm={todo.editorForm}
              />
            ))}
          </List>
    </div>
  );
};

export default observer(TodoList);
