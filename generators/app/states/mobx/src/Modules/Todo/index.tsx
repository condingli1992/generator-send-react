/**
 * TodoApp
 * 
 */

import React, { FunctionComponent } from 'react';
import { inject } from 'mobx-react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import { TodoInstance } from '../../Stores/Todo';
import styles from './index.scss';

export interface TodoProps {
  todo?: TodoInstance;
}

const Todo: FunctionComponent<TodoProps> = ({ todo }) => {
  return (
    <div className={styles.root}>
      <h1 className={styles.headline}>todos</h1>
      <TodoForm todo={todo!} />
      <TodoList todo={todo!}/>
    </div>
  );
};

export default inject(({ appStore }) => ({
  todo: appStore.todo as TodoInstance
}))(Todo);
