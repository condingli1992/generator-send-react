/**
 * AppStore
 * 
 */

import { types, Instance } from 'mobx-state-tree';
import Todo from './Todo';

const AppStore = types
  .model('AppStore')
  .props({
    todo: types.optional(Todo, {})
  });

export default AppStore;
export type AppStoreInstance = Instance<typeof AppStore>;
