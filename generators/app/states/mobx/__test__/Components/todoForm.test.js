import React from 'react';
import { mount } from 'enzyme';
import TodoForm from '../../src/Modules/Todo/TodoForm';

const setup = () => {
  const props = {
    todo: {
      adderForm: { value: '', updateVal: jest.fn() },
      switchAllCompleted: jest.fn(),
      addTodoItem: jest.fn()
    }
  };

  const wrapper = mount(<TodoForm {...props} />);
  return { wrapper, props };
};

describe('TodoForm', () => {
  const { wrapper, props } = setup();

  it('Input是否正确渲染', () => {
    expect(wrapper.find('.ant-input').exists()).toBeTruthy();
  });

  it('Input正确触发onPressEnter事件', () => {
    const mockEvent = { keyCode: 13, target: { value: 'test' } };
    wrapper.find('.ant-input').simulate('keydown', mockEvent);
    expect(props.todo.addTodoItem).toBeCalled();
  });
});
