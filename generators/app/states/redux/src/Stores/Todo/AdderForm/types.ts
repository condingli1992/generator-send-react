/**
 * AdderForm types
 *
 */

export const CHANGE_ADDER_VAL = 'CHANGE_ADDER_VAL';
export const TOGGLE_ALL_TODO = 'TOGGLE_ALL_TODO';

export interface AdderForm {
  value: string;
}

export interface ChangeAdderAction {
  type: typeof CHANGE_ADDER_VAL;
  payload: { value: string };
}

export interface ToggleAllAction {
  type: typeof TOGGLE_ALL_TODO;
  payload: { checked: boolean };
}

export type AdderFormActions = ChangeAdderAction | ToggleAllAction;
