/**
 * AdderForm action
 *
 */

import { CHANGE_ADDER_VAL, TOGGLE_ALL_TODO, ChangeAdderAction, ToggleAllAction } from './types';

export const changeAdder = (value: string): ChangeAdderAction => ({
  type: CHANGE_ADDER_VAL,
  payload: { value }
});

export const toggleAll = (checked: boolean): ToggleAllAction => ({
  type: TOGGLE_ALL_TODO,
  payload: { checked }
});
