/**
 * TodoListStore action
 *
 */

import {
  ADD_TODO,
  TOGGLE_TODO,
  UPDATE_TODO,
  REMOVE_TODO,
  AddTodoAction,
  ToggleTodoAction,
  UpdateTodoAction,
  RemoveTodoAction
} from './types';
import Utils from '../../../Common/Utils';

export const addTodo = (title: string): AddTodoAction => ({
  type: ADD_TODO,
  payload: { id: Utils.uuid(), title, isCompleted: false }
});

export const toggleTodo = (id: string, isCompleted?: boolean): ToggleTodoAction => ({
  type: TOGGLE_TODO,
  payload: { id, isCompleted }
});

export const updateTodo = (id: string, title: string): UpdateTodoAction => ({
  type: UPDATE_TODO,
  payload: { id, title }
});

export const removeTodo = (id: string): RemoveTodoAction => ({
  type: REMOVE_TODO,
  payload: { id }
});
