/**
 * TodoListStore types
 *
 */

export const ADD_TODO = 'ADD_TODO'; // 添加待办项
export const TOGGLE_TODO = 'TOGGLE_TODO'; // 切换待办项
export const UPDATE_TODO = 'UPDATE_TODO'; // 更新待办项
export const REMOVE_TODO = 'REMOVE_TODO'; // 移除待办项

export interface TodoItem {
  id: string;
  title: string;
  isCompleted: boolean;
}

export interface AddTodoAction {
  type: typeof ADD_TODO;
  payload: { id: string; title: string; isCompleted: false };
}

export interface ToggleTodoAction {
  type: typeof TOGGLE_TODO;
  payload: { id: string; isCompleted?: boolean };
}

export interface UpdateTodoAction {
  type: typeof UPDATE_TODO;
  payload: { id: string; title: string };
}

export interface RemoveTodoAction {
  type: typeof REMOVE_TODO;
  payload: { id: string };
}

export type TodoListActions = AddTodoAction | ToggleTodoAction | UpdateTodoAction | RemoveTodoAction;
