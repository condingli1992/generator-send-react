/**
 * EditorFormStore Action
 *
 */

import { CHANGE_EDITOR_VAL, CHANGE_TARGET_ID, ChangeEditorValAction, ChangeTargetIdAction } from './types';

export const changeEditorVal = (value: string): ChangeEditorValAction => ({
  type: CHANGE_EDITOR_VAL,
  payload: { value }
});

export const changeTargetId = (id: string): ChangeTargetIdAction => ({
  type: CHANGE_TARGET_ID,
  payload: { id }
});
