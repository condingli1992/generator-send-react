/**
 * TodoListFooterStore types
 *
 */

export const FILTER_TODO_LIST = 'FILTER_TODO_LIST';
export const CLEAR_COMPLETED_ITEMS = 'CLEAR_COMPLETED_ITEMS';

export enum FilterTypes {
  All = 'All',
  Active = 'Active',
  Completed = 'Completed'
}

export interface TodoListFooter {
  filterType: FilterTypes;
}

export interface SetFilterTypeAction {
  type: string;
  payload: {
    filterType: FilterTypes;
  };
}

export interface ClearCompletedItemsAction {
  type: string;
}

export type TodoListFooterActions = SetFilterTypeAction | ClearCompletedItemsAction;
