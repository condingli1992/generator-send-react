/**
 * TodoListFooter actions
 * 
 */

import { FilterTypes, FILTER_TODO_LIST, CLEAR_COMPLETED_ITEMS } from './types';

export const setFilterType = (filterType: FilterTypes) => ({
  type: FILTER_TODO_LIST,
  payload: { filterType }
});

export const clearCompletedItems = () => ({
  type: CLEAR_COMPLETED_ITEMS
});
