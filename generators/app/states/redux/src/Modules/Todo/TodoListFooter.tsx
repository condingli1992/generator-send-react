/**
 * TodoListFooter
 *
 */

import React, { FunctionComponent } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { FilterTypes } from '../../Stores/Todo/TodoListFooter/types';
import styles from './index.scss';

export interface TodoListFooterProps {
  filterType: FilterTypes;
  listLength: number;
  onSetFilterType: (filterType: FilterTypes) => void;
  onClearCompletedItems: () => void;
}

const TodoListFooter: FunctionComponent<TodoListFooterProps> = ({ listLength, filterType, ...props }) => {
  return (
    <div className={styles.listFooter}>
      <span>
        待办项: <em>{listLength}</em>
      </span>
      <ul className={styles.todoFilters}>
        {Object.keys(FilterTypes).map(item => (
          <li
            key={item}
            className={classnames({ [styles.selectedFilter]: item === filterType.toString() })}
            onClick={() => props.onSetFilterType(item as FilterTypes)}
          >
            {item}
          </li>
        ))}
      </ul>
      <span className={styles.clearBtn} onClick={props.onClearCompletedItems}>
        清除选中项
      </span>
    </div>
  );
};

export default connect()(TodoListFooter);
