# generator-send-react [![NPM version][npm-image]][npm-url] 

> yeoman-generator

## Installation

First, install [Yeoman](http://yeoman.io) and generator-send-react using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-send-react
```

Then generate your new project:

```bash
yo send-react
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).


[npm-image]: https://badge.fury.io/js/generator-send-react.svg
[npm-url]: https://npmjs.org/package/generator-send-react
[travis-image]: https://travis-ci.org//generator-send-react.svg?branch=master
[travis-url]: https://travis-ci.org//generator-send-react
[daviddm-image]: https://david-dm.org//generator-send-react.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-send-react
